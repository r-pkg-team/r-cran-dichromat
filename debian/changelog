r-cran-dichromat (1:2.0-0.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 10:31:39 +0200

r-cran-dichromat (1:2.0-0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 May 2020 17:32:54 +0200

r-cran-dichromat (1:2.0-0-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 31 May 2018 11:25:27 +0200

r-cran-dichromat (1:2.0-0-1) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Use canonical name of CRAN source packages
  * No version mangling of original CRAN version
  * Secure URI in watch file
  * Convert from cdbs to dh-r
  * debhelper 10
  * Remove Ivo Maintz from Uploaders
  * Testsuite: autopkgtest-pkg-r
  * Standards-Version: 4.1.1
  * Canonical homepage for CRAN packages
  * Update d/copyright, license is GPL-2 not GPL-2+
  * Add README.source do document binary data file

 -- Andreas Tille <tille@debian.org>  Fri, 10 Nov 2017 12:39:13 +0100

dichromat (2.0.0-3) unstable; urgency=medium

  * Fix Vcs-Svn
    Closes: #764379
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Tue, 07 Oct 2014 21:37:44 +0200

dichromat (2.0.0-2) unstable; urgency=low

  * debian/doc: Remove empty debhelper file
  * debian/control:
     - Added myself to uploaders
     - normalised formating
     - removed versioned dependency dpkg-dev (>= 1.16.1~)
     - removed unneeded versionen dependency from r-base-dev

 -- Andreas Tille <tille@debian.org>  Fri, 17 May 2013 16:09:23 +0200

dichromat (2.0.0-1) unstable; urgency=low

  * Initial release (Closes: #700854)

 -- Ivo Maintz <ivo@maintz.de>  Thu, 21 Feb 2013 12:41:30 +0100
